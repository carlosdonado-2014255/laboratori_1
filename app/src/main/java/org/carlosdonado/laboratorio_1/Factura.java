package org.carlosdonado.laboratorio_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Factura extends AppCompatActivity {

    private TextView txtPedido;
    private TextView txtTotal;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura);
        txtPedido = (TextView) findViewById(R.id.txt_ped);
        txtTotal = (TextView) findViewById(R.id.txt_total);
        Bundle bundle = this.getIntent().getExtras();
        txtPedido.setText("" + bundle.getString("pedido").toString());
        btnRegresar = (Button) findViewById(R.id.btn_regresar);
        Double total = Double.parseDouble(bundle.getString("pedido"));
        txtTotal.setText("" + total * 7.5);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Factura.this, MainActivity.class));
            }
        });
    }
}
